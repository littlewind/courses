import 'package:courses/src/service/custom_constants.dart';

const Map<String, String> defaultHeader = {
  'Content-Type': 'application/json',
};

const String rootCategoryEndpoint = BASE_URL + rootCategoryPath;
const String subCategoryEndpoint = BASE_URL + subCategoryPath;
const String courseEndpoint = BASE_URL + coursePath;
const String topicEndpoint = BASE_URL + topicPath;
const String cardEndpoint = BASE_URL + cardPath;

const TOPIC_TYPE_LESSON = 1;
const TOPIC_TYPE_EXERCISE = 2;
const TOPIC_TYPE_TEST = 3;