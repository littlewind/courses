import 'package:courses/courses.dart';
import 'package:courses/src/service/course_service.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';

class CourseListModel extends ChangeNotifier {
  late Category category;
  int totalCourses = 0;
  List<Course> courses = [];
  CourseService service = CourseServiceInitializer().courseService;
  bool isLoadingMore = false;
  bool get canLoadMore => courses.length < totalCourses;

  setCategory(Category category) {
    this.category = category;
    totalCourses = category.totalCourses;
  }

  refresh() async {
    final result = await service.getCoursesByCategory(
      ParamGetCoursesByCategory(categoryId: category.id),
    );
    totalCourses = result.totalCourses;
    courses = result.courses;
    // totalCourses = 50;
    // courses = [];
    // courses.addAll(result.courses);
    // courses.addAll(result.courses);
    // courses.addAll(result.courses);
    notifyListeners();
  }

  getMoreCourses({int limit = 10}) async {
    isLoadingMore = true;
    notifyListeners();
    if (!canLoadMore) {
      isLoadingMore = false;
      return;
    }
    // final result = await service.getCoursesByCategory(
    //   ParamGetCoursesByCategory(
    //     categoryId: category.id,
    //     lastCourseId: courses.last.id,
    //     lastFieldName: 'name',
    //     lastFieldValue: courses.last.name,
    //     limit: limit,
    //   ),
    // );
    // courses.addAll(result.courses);
    late int end;
    if (courses.length < 10) {
      end = courses.length;
    } else {
      end = 10;
    }
    final result = courses.sublist(0, end);
    courses.addAll(result);
    print('Phungtd: getMoreCourses: new Length: ${courses.length}');
    isLoadingMore = false;
    notifyListeners();
  }
}
