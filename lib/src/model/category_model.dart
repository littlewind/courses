import 'package:courses/src/service/course_service.dart';
import 'package:courses/src/service/service.dart';
import 'package:flutter/cupertino.dart';
import 'package:models/module_models.dart';

class CategoryModel extends ChangeNotifier {
  CourseService service = CourseServiceInitializer().courseService;
  List<Category> categories = [];
  List<Category> subCategories = [];
  Category? selectedCategory;

  getCategories() async {
    print('Phungtd: getCategories()');
    // categories = [
    //   Category(
    //     id: 'id',
    //     avatar: '',
    //     name: 'Arts and Humanities',
    //     description: 'description',
    //     shortDescription: 'shortDescription',
    //     slug: 'slug',
    //     titleSEO: 'titleSEO',
    //     seo301: 'seo301',
    //     seoRobot: 'seoRobot',
    //     descriptionSeo: 'descriptionSeo',
    //     type: 0,
    //     timeStudy: 120,
    //     status: 0,
    //     lastUpdate: 0,
    //     price: 50000,
    //     discountPrice: 50000,
    //     index: 0,
    //     totalCourses: 10,
    //   ),
    //   Category(
    //     id: 'id2',
    //     avatar: '',
    //     name: 'Business',
    //     description: 'description',
    //     shortDescription: 'shortDescription',
    //     slug: 'slug',
    //     titleSEO: 'titleSEO',
    //     seo301: 'seo301',
    //     seoRobot: 'seoRobot',
    //     descriptionSeo: 'descriptionSeo',
    //     type: 0,
    //     timeStudy: 120,
    //     status: 0,
    //     lastUpdate: 0,
    //     price: 50000,
    //     discountPrice: 50000,
    //     index: 0,
    //     totalCourses: 10,
    //   ),
    //   Category(
    //     id: 'id2',
    //     avatar: '',
    //     name: 'Computer Science',
    //     description: 'description',
    //     shortDescription: 'shortDescription',
    //     slug: 'slug',
    //     titleSEO: 'titleSEO',
    //     seo301: 'seo301',
    //     seoRobot: 'seoRobot',
    //     descriptionSeo: 'descriptionSeo',
    //     type: 0,
    //     timeStudy: 120,
    //     status: 0,
    //     lastUpdate: 0,
    //     price: 50000,
    //     discountPrice: 50000,
    //     index: 0,
    //     totalCourses: 10,
    //   ),
    // ];

    categories = await service.getRootCategories();

    if (categories.isNotEmpty) {
      selectedCategory = categories.first;
      if (categories.first.totalCourses > 0) {
        getSubCategories(categories.first.id);
      }
    }

    // subCategories = [
    //   dummySubCategory,
    //   dummySubCategory,
    //   dummySubCategory,
    //   dummySubCategory,
    //   // dummySubCategory,
    // ];
    // notifyListeners();
  }

  selectCategory(Category? category) {
    selectedCategory = category;
    notifyListeners();
  }

  getSubCategories(String parentId) async {
    final result = await service.getSubCategoriesByParentId(parentId);
    subCategories = result.where((e) => e.totalCourses > 0).toList();
    // notifyListeners();
    await Future.forEach<Category>(subCategories, (category) async {
      final courseList = await getCoursesByCategory(category.id);
      category.courses = courseList;
      print('Phungtd: foreach -- getCoursesByCategory -- ');
      // notifyListeners();
    });
    // print('Phungtd: Before notify: subCategories.first.courses.length: ${subCategories.first.courses.length}');
    // final List<Category> newList = [];
    // newList.addAll(subCategories);
    // subCategories = newList;
    notifyListeners();
  }

  Future<List<Course>> getCoursesByCategory(String categoryId) async {
    final result = await service.getCoursesByCategory(
        ParamGetCoursesByCategory(categoryId: categoryId));
    return result.courses;
  }
}

final dummySubCategory = Category(
  id: 'id3',
  avatar: '',
  name: 'SUBBB Arts and Humanities',
  description: 'description',
  shortDescription: 'shortDescription',
  slug: 'slug',
  titleSEO: 'titleSEO',
  seo301: 'seo301',
  seoRobot: 'seoRobot',
  descriptionSeo: 'descriptionSeo',
  type: 0,
  timeStudy: 120,
  status: 0,
  lastUpdate: 0,
  price: 50000,
  discountPrice: 50000,
  index: 0,
  totalCourses: 10,
  courses: [
    Course(
      id: 'id1',
      name: 'Graphic Design Graphic Design Graphic Design Graphic Design',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id2',
      name: 'Creative Writing',
      shortDesc: 'shortDesc',
      avatar:
          'https://i.pinimg.com/originals/eb/a8/e8/eba8e8a51e9692156cadc66446672a49.png',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id3',
      name: 'Creative Reading',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id4',
      name: 'Creative Design',
      shortDesc: 'shortDesc',
      avatar:
          'https://i.pinimg.com/originals/eb/a8/e8/eba8e8a51e9692156cadc66446672a49.png',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id5',
      name: 'Art Design',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id6',
      name: 'Game Design',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
  ],
);
final dummySubCategory2 = Category(
  id: 'id3',
  avatar: '',
  name: 'SUBBB Arts and Humanities',
  description: 'description',
  shortDescription: 'shortDescription',
  slug: 'slug',
  titleSEO: 'titleSEO',
  seo301: 'seo301',
  seoRobot: 'seoRobot',
  descriptionSeo: 'descriptionSeo',
  type: 0,
  timeStudy: 120,
  status: 0,
  lastUpdate: 0,
  price: 50000,
  discountPrice: 50000,
  index: 0,
  totalCourses: 10,
  courses: [
    Course(
      id: 'id1',
      name: 'Graphic Design Graphic Design Graphic Design Graphic Design',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id2',
      name: 'Creative Writing',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id3',
      name: 'Creative Reading',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id4',
      name: 'Creative Design',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id5',
      name: 'Art Design',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
    Course(
      id: 'id6',
      name: 'Game Design',
      shortDesc: 'shortDesc',
      avatar: 'https://sieupet.com/sites/default/files/shiba_inu2_0.jpg',
      startTime: 0,
      endTime: 0,
      cost: 0,
      discountPercent: 0,
      discountPrice: 0,
      privacy: 0,
      type: 0,
      status: 0,
      password: 'password',
      slug: 'slug',
      courseSystemId: 'courseSystemId',
      courseContentId: 'courseContentId',
      ownerId: 'ownerId',
      categoryId: 'categoryId',
    ),
  ],
);
