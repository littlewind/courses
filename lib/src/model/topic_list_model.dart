import 'package:courses/src/constants.dart';
import 'package:courses/src/service/course_service.dart';
import 'package:courses/src/service/service.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';

class TopicListModel extends ChangeNotifier {
  List<Topic> topics = const [];

  CourseService service = CourseServiceInitializer().courseService;

  getTopics(Course course, String uid) async {
    final topicList = await service.getTopicsByCourseId(course.id, uid);
    await Future.forEach<Topic>(
        topicList, (topic) => getSubTopics(topic, course.id));
    topics = topicList;
    notifyListeners();
  }

  getSubTopics(Topic topic, String courseId) async {
    if (topic.type != TOPIC_TYPE_LESSON || topic.childType == TOPIC_TYPE_CHILD_NONE) {
      return;
    }
    final subTopicList = await service.getTopicsByParentId(courseId, topic.id);
    topic.children = subTopicList;
    if (subTopicList.isNotEmpty) {
      await Future.forEach<Topic>(
          subTopicList, (subTopic) => getSubTopics(subTopic, courseId));
    }
  }
}
