import 'dart:async';

import 'package:models/module_models.dart';

abstract class CourseService {
  Future<List<Category>> getRootCategories();

  Future<List<Category>> getSubCategoriesByParentId(String parentId);

  Future<GetCoursesByCategoryModel> getCoursesByCategory(
    ParamGetCoursesByCategory params);

  Future<List<Topic>> getTopicsByCourseId(String courseId, String userId);

  Future<List<Topic>> getTopicsByParentId(String courseId, String parentId);

  goToExerciseScreen(Topic topic);

  // Future<List<Card>> getCardsByTopicId(String topicId);
}

class ParamGetCoursesByCategory {
  String categoryId;
  String? lastCourseId;
  String lastFieldName;
  dynamic? lastFieldValue;
  int limit;
  bool asc;

  ParamGetCoursesByCategory({
    required this.categoryId,
    this.lastCourseId,
    this.lastFieldName = 'name',
    this.lastFieldValue,
    this.limit = 10,
    this.asc = true,
  });
}

class GetCoursesByCategoryModel {
  int totalCourses;
  List<Course> courses;

  GetCoursesByCategoryModel({
    this.totalCourses = 0,
    this.courses = const [],
  });
}
