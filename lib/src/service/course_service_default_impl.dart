import 'dart:convert';

import 'package:courses/src/constants.dart';
import 'package:courses/src/service/course_service.dart';
import 'package:models/module_models.dart';
import 'package:http/http.dart' as http;

class CourseServiceDefaultImpl implements CourseService {
  @override
  Future<List<Category>> getRootCategories() async {
    final response = await http.post(Uri.parse(rootCategoryEndpoint));
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body);
      final coursesListJson = json['data'] as List;
      List<Category> result = [];
      result = coursesListJson.map((e) => Category.fromJson(e)).toList();
      return result;
    }
    return [];
  }

  @override
  Future<List<Category>> getSubCategoriesByParentId(String parentId) async {
    final body = jsonEncode({
      'parentId': parentId,
    });
    final response = await http.post(
      Uri.parse(subCategoryEndpoint),
      headers: defaultHeader,
      body: body,
    );
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body);
      final coursesListJson = json['data'] as List;
      List<Category> result = [];
      result = coursesListJson.map((e) => Category.fromJson(e)).toList();
      print(
          'Phungtd: getSubCategoriesByParentId: $parentId -> result length: ${result.length}');
      return result;
    }
    return [];
  }

  @override
  Future<GetCoursesByCategoryModel> getCoursesByCategory(
      ParamGetCoursesByCategory params) async {
    final lastRecord = params.lastCourseId == null
        ? null
        : {
            "_id": params.lastCourseId,
            params.lastFieldName: params.lastFieldValue,
          };
    final body = jsonEncode({
      "field": params.lastFieldName,
      "limit": params.limit,
      "asc": params.asc,
      "lastRecord": lastRecord,
      "categoryId": params.categoryId,
    });
    final response = await http.post(
      Uri.parse(courseEndpoint),
      headers: defaultHeader,
      body: body,
    );
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body);
      final modelJson = json['data'];
      final int totalCourses = modelJson['totalCourses'];
      final coursesListJson = modelJson['courses'] as List;
      List<Course> courseList = [];
      courseList = coursesListJson.map((e) => Course.fromJson(e)).toList();
      print(
          'Phungtd: getCoursesByCategory: ${params.categoryId} -> result length: ${courseList.length}');
      return GetCoursesByCategoryModel(
        totalCourses: totalCourses,
        courses: courseList,
      );
    }
    return GetCoursesByCategoryModel();
  }

  @override
  Future<List<Topic>> getTopicsByCourseId(
      String courseId, String userId) async {
    final body = jsonEncode({
      "courseId": courseId,
      "userId": userId,
    });
    final response = await http.post(
      Uri.parse(topicEndpoint),
      headers: defaultHeader,
      body: body,
    );
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body) as List;
      final List<Topic> topicList = json.map((e) => Topic.fromJson(e)).toList();
      return topicList;
    }
    return [];
  }

  @override
  Future<List<Topic>> getTopicsByParentId(
      String courseId, String parentId) async {
    final body = jsonEncode({
      "courseId": courseId,
      "parentId": parentId,
    });
    final response = await http.post(
      Uri.parse(topicEndpoint),
      headers: defaultHeader,
      body: body,
    );
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body) as List;
      final List<Topic> topicList = json.map((e) => Topic.fromJson(e)).toList();
      return topicList;
    }
    return [];
  }

/*  @override
  Future<List<Card>> getCardsByTopicId(String topicId) async {
    final body = jsonEncode({
      "topicId": topicId,
    });
    final response = await http.post(
      Uri.parse(cardEndpoint),
      headers: defaultHeader,
      body: body,
    );
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body) as List;
      final List<Card> cardList = json.map((e) => Card.fromJson(e)).toList();
      return cardList;
    }
    return [];
  }*/

  @override
  goToExerciseScreen(Topic topic) {}
}
