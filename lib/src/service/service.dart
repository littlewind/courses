import 'package:courses/src/service/course_service.dart';

class CourseServiceInitializer {
  factory CourseServiceInitializer() {
    if (_instance == null) {
      _instance = CourseServiceInitializer._getInstance();
    }
    return _instance!;
  }

  static CourseServiceInitializer? _instance;
  CourseServiceInitializer._getInstance();

  late final CourseService courseService;

  init(CourseService courseService) {
    this.courseService = courseService;
  }
}