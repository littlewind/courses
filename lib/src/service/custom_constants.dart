const String BASE_URL = 'http://103.226.248.62:6001/api/';

const String rootCategoryPath = 'get-root-categories';
const String subCategoryPath = 'get-categories-by-parent';
const String coursePath = 'get-paged-courses-by-category';
const String topicPath = 'get-topic-by-parent-id';
const String cardPath = 'get-card-by-topic-id';