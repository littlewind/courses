import 'package:courses/src/model/category_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreenLogic {
  BuildContext context;

  HomeScreenLogic(this.context);

  void getCategories() {
    context.read<CategoryModel>().getCategories();
  }

  void getSubCategories(String parentId) {
    context.read<CategoryModel>().getSubCategories(parentId);
  }
}
