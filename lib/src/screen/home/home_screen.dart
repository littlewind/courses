import 'package:courses/src/model/category_model.dart';
import 'package:courses/src/screen/course/course_list_screen.dart';
import 'package:courses/src/screen/home/home_screen_logic.dart';
import 'package:courses/src/screen/topic/topic_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeScreenLogic logic;

  @override
  void initState() {
    super.initState();
    logic = HomeScreenLogic(context);
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      logic.getCategories();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            buildCategoryList(),
            SizedBox(height: 16.0),
            Expanded(child: buildSubCategoryList()),
          ],
        ),
      ),
    );
  }

  Widget buildCategoryList() {
    return Consumer<CategoryModel>(
      builder: (_, model, __) {
        if (model.categories.isEmpty) return Container();
        return DropdownButtonHideUnderline(
          child: DropdownButton<Category>(
            items: model.categories
                .map((category) => DropdownMenuItem<Category>(
                      value: category,
                      child: Text(category.name),
                    ))
                .toList(),
            value: model.selectedCategory,
            onChanged: (category) {
              if (model.selectedCategory == category) {
                return;
              }
              setState(() {
                model.selectCategory(category);
              });
              if ((category?.totalCourses ?? 0) > 0) {
                logic.getSubCategories(category!.id);
              }
            },
          ),
        );
      },
    );
  }

  Widget buildSubCategoryList() {
    return Selector<CategoryModel, List<Category>>(
      selector: (_, model) => model.subCategories,
      // builder: (_, categories, __) => CustomScrollView(
      //   slivers:
      //       categories.map((category) => buildSubCategory(category)).toList(),
      // ),
      builder: (_, categories, __) {
        print('Phungtd: HomeScreen -> buildSubCategoryList: rebuild');
        return SingleChildScrollView(
          child: Column(
            children: categories
                .map((category) => buildSubCategory(category))
                .toList(),
          ),
        );
      },
    );
  }

  Widget buildSubCategory(Category category) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                category.name,
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            SizedBox(
              width: 16.0,
            ),
            InkWell(
              onTap: () {
                _goToListCourseScreen(category);
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('See All'),
              ),
            ),
          ],
        ),
        SizedBox(height: 12.0),
        Container(
          height: 220,
          child: ListView.separated(
            // shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            separatorBuilder: (_, __) => SizedBox(width: 8.0),
            itemCount:
                category.courses.length == 0 ? 3 : category.courses.length,
            itemBuilder: (_, index) => category.courses.length == 0
                ? buildCourseOverViewPlaceHolder()
                : buildCourseOverView(category.courses[index]),
          ),
        ),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget buildCourseOverView(Course course) {
    return GestureDetector(
      onTap: () {
        _goToListTopicScreen(course);
      },
      child: Container(
        // color: Colors.blue,
        width: 150,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              // color: Colors.red,
              width: double.infinity,
              height: 150.0,
              child: course.avatar.isEmpty
                  ? Image.asset(
                      'assets/images/hat.png',
                      package: 'courses',
                    )
                  : Image.network(
                      course.avatar,
                      fit: BoxFit.fill,
                    ),
            ),
            // SizedBox(height: 8.0),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    course.name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.w600),
                  ),
                  SizedBox(height: 8.0),
                  Text(course.shortDesc,
                      maxLines: 1, overflow: TextOverflow.ellipsis),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCourseOverViewPlaceHolder() {
    return Shimmer.fromColors(
      baseColor: Colors.grey.withOpacity(0.8),
      highlightColor: Colors.white70,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 150,
            height: 150.0,
            color: Colors.grey,
          ),
          SizedBox(height: 8.0),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 16,
                  width: 80,
                  color: Colors.grey,
                ),
                SizedBox(height: 8.0),
                Container(
                  height: 12,
                  width: 120,
                  color: Colors.grey,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _goToListCourseScreen(Category category) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => CourseListScreen(category: category)),
    );
  }

  _goToListTopicScreen(Course course) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => TopicListScreen(course)),
    );
  }
}
