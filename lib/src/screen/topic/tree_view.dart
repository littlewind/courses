import 'package:flutter/material.dart';

class TreeNodeData {
  final Widget parent;
  final VoidCallback? onTapParent;
  final List<TreeNodeData> children;

  TreeNodeData({
    required this.parent,
    this.onTapParent,
    this.children = const [],
  });
}

class TreeView extends StatefulWidget {
  final List<TreeNodeData> children;

  TreeView({
    Key? key,
    required this.children,
  }) : super(key: key);

  @override
  _TreeViewState createState() => _TreeViewState();
}

class _TreeViewState extends State<TreeView> {
  List<TreeNodeData> get children => widget.children;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: children.length,
      itemBuilder: (_, index) => TreeNode(children[index]),
    );
  }
}

class TreeNode extends StatefulWidget {
  final TreeNodeData data;

  TreeNode(this.data);

  @override
  _TreeNodeState createState() => _TreeNodeState();
}

class _TreeNodeState extends State<TreeNode> {
  TreeNodeData get data => widget.data;
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            if (data.onTapParent != null) {
              data.onTapParent!();
            } else {
              toggleExpanded();
            }
          },
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(child: data.parent),
                      if (data.children.isNotEmpty) buildExpandedIcon(),
                    ],
                  ),
                ),
              ),
              if (data.children.isNotEmpty && isExpanded)
                Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: data.children.length,
                    itemBuilder: (_, index) => TreeNode(data.children[index]),
                  ),
                ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildExpandedIcon() {
    return isExpanded
        ? Icon(Icons.keyboard_arrow_down)
        : Icon(Icons.keyboard_arrow_right);
  }

  void toggleExpanded() {
    setState(() {
      this.isExpanded = !this.isExpanded;
    });
  }
}
