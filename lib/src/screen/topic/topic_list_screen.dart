import 'package:courses/src/constants.dart';
import 'package:courses/src/model/topic_list_model.dart';
import 'package:courses/src/screen/topic/tree_view.dart';
import 'package:courses/src/service/course_service.dart';
import 'package:courses/src/service/service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:models/module_models.dart';

class TopicListScreen extends StatefulWidget {
  final Course course;

  TopicListScreen(this.course);

  @override
  _TopicListScreenState createState() => _TopicListScreenState();
}

class _TopicListScreenState extends State<TopicListScreen> {
  CourseService service = CourseServiceInitializer().courseService;
  Course get course => widget.course;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      final uid = context.read<AppSettingModel>().appSetting?.userId ?? '';
      context.read<TopicListModel>().getTopics(course, uid);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(course.name),
      ),
      body: Column(
        children: [
          SizedBox(height: 16.0),
          Text(
            'DANH SÁCH KHÓA HỌC',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Container(
              height: 1,
              width: double.infinity,
              color: Colors.black,
            ),
          ),
          Expanded(
            child: Consumer<TopicListModel>(builder: (_, model, __) {
              if (model.topics.isNotEmpty) {
                return TreeView(
                    children: model.topics
                        .map((topic) =>
                            buildTreeNodeFromTopic(topic, isRoot: true))
                        .toList());
              }
              return Center(child: CircularProgressIndicator());
            }),
          ),
        ],
      ),
    );
  }

  TreeNodeData buildTreeNodeFromTopic(Topic topic, {bool isRoot = false}) {
    final parent = Row(
      children: [
        SizedBox(width: 8.0),
        getTopicTypeIcon(topic),
        SizedBox(width: 8.0),
        Expanded(
          child: Text(
            topic.name,
            style: TextStyle(
                fontWeight: isRoot ? FontWeight.bold : FontWeight.normal),
          ),
        ),
      ],
    );

    return TreeNodeData(
      parent: parent,
      onTapParent: onTapTopic(topic),
      children:
          topic.children.map((topic) => buildTreeNodeFromTopic(topic)).toList(),
    );
  }

  Widget getTopicTypeIcon(Topic topic) {
    return Icon(Icons.book);
  }

  VoidCallback? onTapTopic(Topic topic) {
    if (topic.children.isNotEmpty ) {
      return null;
    }  
    switch(topic.type) {
      case TOPIC_TYPE_EXERCISE:
        return () {
          goToExerciseScreen(topic);
        };
      default:
        return null;
    }
  }

  void goToExerciseScreen(Topic topic) {
    service.goToExerciseScreen(topic);
  }
}
