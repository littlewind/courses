import 'package:courses/src/model/course_list_model.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';
import 'package:provider/provider.dart';

class CourseListScreen extends StatefulWidget {
  final Category category;

  CourseListScreen({Key? key, required this.category}) : super(key: key);

  @override
  _CourseListScreenState createState() => _CourseListScreenState();
}

class _CourseListScreenState extends State<CourseListScreen> {
  Category get category => widget.category;
  ScrollController controller = ScrollController();

  @override
  void initState() {
    super.initState();
    final model = context.read<CourseListModel>();
    model.setCategory(category);
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      model.refresh();
      controller.addListener(_scrollListener);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(category.name, overflow: TextOverflow.ellipsis),
      ),
      body: CustomScrollView(
        controller: controller,
        slivers: [
          Consumer<CourseListModel>(builder: (_, model, __) {
            return SliverList(
              delegate: SliverChildBuilderDelegate(
                (_, index) => buildCourseOverView(model.courses[index]),
                childCount: model.courses.length,
              ),
            );
          }),
          Consumer<CourseListModel>(
              builder: (_, model, __) => buildSliverLoading(model.canLoadMore)),
        ],
      ),
    );
  }

  Widget buildCourseOverView(Course course) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 6.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  course.name,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
                SizedBox(height: 8.0),
                Text(course.shortDesc),
              ],
            ),
          ),
          SizedBox(width: 12.0),
          course.avatar.isEmpty
              ? Image.asset('assets/images/hat.png',
                  package: 'courses', height: 100, width: 100)
              : Image.network(course.avatar, height: 100, width: 100)
        ],
      ),
    );
  }

  void _scrollListener() {
    if (controller.offset >= controller.position.maxScrollExtent &&
        !controller.position.outOfRange) {
      bool isLoading = context.read<CourseListModel>().isLoadingMore;
      if (!isLoading) {
        context.read<CourseListModel>().getMoreCourses();
      }
    }
  }

  Widget buildSliverLoading(bool isLoading) {
    return SliverToBoxAdapter(
      child:
          isLoading ? Center(child: CircularProgressIndicator()) : Container(),
    );
  }
}
